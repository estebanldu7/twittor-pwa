const fs = require("fs");
const urlSafe = require("urlsafe-base64");
const vapid = require("./vapid.json");
let subscriptions = require("./subs-db.json");
const webpush = require("web-push");

webpush.setGCMAPIKey("<Your GCM API Key Here>");
webpush.setVapidDetails(
  "mailto:epreciado@linux.com",
  vapid.publicKey,
  vapid.privateKey
);

module.exports.getKey = () => {
  return urlSafe.decode(vapid.publicKey);
};

module.exports.addSubscription = (suscription) => {
  subscriptions.push(suscription);
  fs.writeFileSync(`${__dirname}/subs-db.json`, JSON.stringify(subscriptions));
};

module.exports.sendPush = (post) => {
  const sentNotifications = [];

  subscriptions.forEach((suscription, i) => {
    const pushProm = webpush
      .sendNotification(suscription, JSON.stringify(post))
      .then(console.log("Notificacion Enviada"))
      .catch((err) => {
        if (err.statusCode === 410) {
          subscriptions[i].delete = true;
        }
      });

    sentNotifications.push(pushProm);
  });

  Promise.all(sentNotifications).then(() => {
    subscriptions = subscriptions.filter((subs) => !subs.delete);
    fs.writeFileSync(
      `${__dirname}/subs-db.json`,
      JSON.stringify(subscriptions)
    );
  });
};
