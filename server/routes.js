// Routes.js - Módulo de rutas
const express = require("express");
const router = express.Router();
const push = require("./push");

const mensajes = [
  {
    _id: 1,
    user: "spiderman",
    message: "Hello World",
  },
  {
    _id: 2,
    user: "ironman",
    message: "Hello World",
  },
];

// Get mensajes
router.get("/", function (req, res) {
  res.json(mensajes);
});

// post message
router.post("/", function (req, res) {
  const message = {
    message: req.body.message,
    user: req.body.user,
    lat: req.body.lat,
    lng: req.body.lng,
    photo: req.body.foto,
  };

  mensajes.push(message);

  console.log(mensajes);

  res.json({
    ok: true,
    message,
  });
});

router.post("/subscribe", (req, res) => {
  const suscription = req.body;

  push.addSubscription(suscription);

  res.json("suscribe");
});

router.get("/key", (req, res) => {
  const key = push.getKey();
  res.send(key);
});

router.post("/push", (req, res) => {
  const notification = {
    title: req.body.title,
    body: req.body.body,
    user: req.body.user,
  };

  push.sendPush(notification);
});

module.exports = router;
