var url = window.location.href;
var swLocation = "/twittor/sw.js";

if (navigator.serviceWorker) {
  if (url.includes("localhost")) {
    swLocation = "/sw.js";
  }

  window.addEventListener("load", function () {
    navigator.serviceWorker.register(swLocation).then(function (reg) {
      swReg = reg;
      swReg.pushManager.getSubscription().then(verifySuscription);
    });
  });
}

// Referencias de jQuery
var googleMapKey = "AIzaSyA5mjCwx1TRLuBAjwQw84WE6h5ErSe7Uj8";

// Google Maps llaves alternativas - desarrollo
// AIzaSyDyJPPlnIMOLp20Ef1LlTong8rYdTnaTXM
// AIzaSyDzbQ_553v-n8QNs2aafN9QaZbByTyM7gQ
// AIzaSyA5mjCwx1TRLuBAjwQw84WE6h5ErSe7Uj8
// AIzaSyCroCERuudf2z02rCrVa6DTkeeneQuq8TA
// AIzaSyBkDYSVRVtQ6P2mf2Xrq0VBjps8GEcWsLU
// AIzaSyAu2rb0mobiznVJnJd6bVb5Bn2WsuXP2QI
// AIzaSyAZ7zantyAHnuNFtheMlJY1VvkRBEjvw9Y
// AIzaSyDSPDpkFznGgzzBSsYvTq_sj0T0QCHRgwM
// AIzaSyD4YFaT5DvwhhhqMpDP2pBInoG8BTzA9JY
// AIzaSyAbPC1F9pWeD70Ny8PHcjguPffSLhT-YF8

var titulo = $("#titulo");
var nuevoBtn = $("#nuevo-btn");
var salirBtn = $("#salir-btn");
var cancelarBtn = $("#cancel-btn");
var postBtn = $("#post-btn");
var avatarSel = $("#seleccion");
var timeline = $("#timeline");

var modal = $("#modal");
var modalAvatar = $("#modal-avatar");
var avatarBtns = $(".seleccion-avatar");
var txtMensaje = $("#txtMensaje");

var btnActivadas = $(".btn-noti-activadas");
var btnDesactivadas = $(".btn-noti-desactivadas");

var btnLocation = $("#location-btn");

var modalMapa = $(".modal-mapa");

var btnTomarFoto = $("#tomar-foto-btn");
var btnPhoto = $("#photo-btn");
var contenedorCamara = $(".camara-contenedor");

var lat = null;
var lng = null;
var foto = null;

// El usuario, contiene el ID del hÃ©roe seleccionado
var usuario;

//init camera
const camera = new Camera($("#player")[0]);

// ===== Codigo de la aplicaciÃ³n

function crearMensajeHTML(mensaje, personaje, lat, lng, foto) {
  var content = `
    <li class="animated fadeIn fast"
        data-user="${personaje}"
        data-mesaje="${mensaje}"
        data-tip="mensaje">
        <div class="avatar">
            <img src="img/avatars/${personaje}.jpg">
        </div>
        <div class="bubble-container">
            <div class="bubble">
                <h3>@${personaje}</h3>
                <br/>
                ${mensaje}
            </div>
            
            <div class="arrow"></div>
        </div>
    </li>
    `;

  if (foto) {
    content += `
                <br>
                <img class="foto-mensaje" src="${foto}">
        `;
  }

  content += `</div>        
                <div class="arrow"></div>
            </div>
        </li>
    `;

  // si existe la latitud y longitud,
  // llamamos la funcion para crear el mapa
  if (lat) {
    crearMensajeMapa(lat, lng, personaje);
  }

  // Borramos la latitud y longitud por si las usó
  lat = null;
  lng = null;

  $(".modal-mapa").remove();

  timeline.prepend(content);
  cancelarBtn.click();
}

function crearMensajeMapa(lat, lng, personaje) {
  let content = `
    <li class="animated fadeIn fast"
        data-tipo="mapa"
        data-user="${personaje}"
        data-lat="${lat}"
        data-lng="${lng}">
                <div class="avatar">
                    <img src="img/avatars/${personaje}.jpg">
                </div>
                <div class="bubble-container">
                    <div class="bubble">
                        <iframe
                            width="100%"
                            height="250"
                            frameborder="0" style="border:0"
                            src="https://www.google.com/maps/embed/v1/view?key=${googleMapKey}&center=${lat},${lng}&zoom=17" allowfullscreen>
                            </iframe>
                    </div>
                    
                    <div class="arrow"></div>
                </div>
            </li> 
    `;

  timeline.prepend(content);
}

// Globals
function logIn(ingreso) {
  if (ingreso) {
    nuevoBtn.removeClass("oculto");
    salirBtn.removeClass("oculto");
    timeline.removeClass("oculto");
    avatarSel.addClass("oculto");
    modalAvatar.attr("src", "img/avatars/" + usuario + ".jpg");
  } else {
    nuevoBtn.addClass("oculto");
    salirBtn.addClass("oculto");
    timeline.addClass("oculto");
    avatarSel.removeClass("oculto");

    titulo.text("Seleccione Personaje");
  }
}

// Seleccion de personaje
avatarBtns.on("click", function () {
  usuario = $(this).data("user");

  titulo.text("@" + usuario);

  logIn(true);
});

// Boton de salir
salirBtn.on("click", function () {
  logIn(false);
});

// Boton de nuevo mensaje
nuevoBtn.on("click", function () {
  modal.removeClass("oculto");
  modal.animate(
    {
      marginTop: "-=1000px",
      opacity: 1,
    },
    200
  );
});

// Boton de cancelar mensaje
cancelarBtn.on("click", function () {
  if (!modal.hasClass("oculto")) {
    modal.animate(
      {
        marginTop: "+=1000px",
        opacity: 0,
      },
      200,
      function () {
        modal.addClass("oculto");
        txtMensaje.val("");
      }
    );
  }
});

// Boton de enviar mensaje
postBtn.on("click", function () {
  var mensaje = txtMensaje.val();
  if (mensaje.length === 0) {
    cancelarBtn.click();
    return;
  }

  const data = {
    message: mensaje,
    user: usuario,
    lat: lat,
    lng: lng,
    foto: foto,
  };

  fetch("api", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  })
    .then((res) => res.json())
    .then((res) => console.log("app.js", res))
    .catch((err) => console.log("app.js", err));

  crearMensajeHTML(mensaje, usuario, lat, lng, foto);

  foto = null;
});

// Get message from server
function getMessages() {
  fetch("api")
    .then((resp) => resp.json())
    .then((posts) => {
      console.log(posts);

      posts.forEach((post) => {
        crearMensajeHTML(post.message, post.user);
      });
    });
}

getMessages();

//Verify online mode
function isOnlie() {
  if (navigator.onLine) {
    $.mdtoast("Online", {
      interaction: true,
      interactionTime: 1000,
      actionText: "OK",
    });
  } else {
    $.mdtoast("Offline", {
      interaction: true,
      actionText: "OK",
      type: "warning",
    });
  }
}

window.addEventListener("online", isOnlie);
window.addEventListener("offline", isOnlie);

isOnlie();

function verifySuscription(isActive) {
  if (isActive) {
    btnActivadas.removeClass("oculto");
    btnDesactivadas.addClass("oculto");
  } else {
    btnActivadas.addClass("oculto");
    btnDesactivadas.removeClass("oculto");
  }
}

// Notifications
function notifyme() {
  if (!window.Notification) {
  }

  if (Notification.permission === "granted") {
    console.log("Hello World");
  } else if (
    Notification.permission !== "denied" ||
    Notification.permission === "default"
  ) {
    Notification.requestPermission(function (permission) {
      if (permission === "granted") {
        console.log("Hello World from question");
      }
    });
  }
}

notifyme();

//get Key
function getPublickey() {
  // fetch("api/key")
  //   .then((resp) => resp.text())
  //   .then(console.log);

  return fetch("api/key")
    .then((resp) => resp.arrayBuffer())
    .then((key) => new Uint8Array(key));
}

btnDesactivadas.on("click", function () {
  if (!swReg) return console.log("No hay registro de SW");
  getPublickey().then(function (key) {
    swReg.pushManager
      .subscribe({
        userVisibleOnly: true,
        applicationServerKey: key,
      })
      .then((res) => res.toJSON())
      .then((suscription) => {
        fetch("api/subscribe", {
          method: "POST",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify(suscription),
        })
          .then(verifySuscription)
          .catch(console.log);
        verifySuscription(suscription);
      });
  });
});

function cancelSubscription() {
  swReg.pushManager.getSubscription().then((subs) => {
    subs.unsubscribe().then(() => verifySuscription(false));
  });
}

btnActivadas.on("click", function () {
  cancelSubscription();
});

// Crear mapa en el modal
function mostrarMapaModal(lat, lng) {
  $(".modal-mapa").remove();

  var content = `
            <div class="modal-mapa">
                <iframe
                    width="100%"
                    height="250"
                    frameborder="0"
                    src="https://www.google.com/maps/embed/v1/view?key=${googleMapKey}&center=${lat},${lng}&zoom=17" allowfullscreen>
                    </iframe>
            </div>
    `;

  modal.append(content);
}

// Obtener la geolocalización
btnLocation.on("click", () => {
  $.mdtoast("Cargando Mapa", {
    interaction: true,
    interactionTimeout: 2000,
    actionText: "Ok",
  });

  navigator.geolocation.getCurrentPosition((pos) => {
    mostrarMapaModal(pos.coords.latitude, pos.coords.longitude);
    lat = pos.coords.latitude;
    lng = pos.coords.longitude;
  });
});

// Boton de la camara
// usamos la funcion de fleca para prevenir
// que jQuery me cambie el valor del this
btnPhoto.on("click", () => {
  contenedorCamara.removeClass("oculto");
  camera.turnOn();
});

// Boton para tomar la foto
btnTomarFoto.on("click", () => {
  foto = camera.takePhoto();
  camera.turnOff();
});

// Share API

if (navigator.share) {
  console.log("Funciona");
} else {
  console.log("No Funciona");
}

timeline.on("click", "li", function () {
  let type = $(this).data("tipo");
  let lat = $(this).data("lat");
  let lng = $(this).data("lng");
  let message = $(this).data("mensaje");
  let user = $(this).data("user");

  const shareOpt = {
    title: user,
    text: message,
  };

  if (type === "mapa") {
    shareOpt.text = "Mapa";
    shareOpt.url = `https://www.google.com/maps/@${lat},${lng},15`;
  }

  navigator
    .share({
      title: user,
      text: message,
      url: "",
    })
    .then(() => console.log("Funciona"))
    .catch(() => console.log("No Funciona"));
});
