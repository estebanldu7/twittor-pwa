//Save posts in base pouchDB

const db = new PouchDB("mensajes");

function guardarMensaje(mensaje) {
  mensaje._id = new Date().toISOString();
  return db.put(mensaje).then(() => {
    self.registration.sync.register("new-post");
    const newResp = { ok: true, offline: true };
    return new Response(JSON.stringify(newResp));
  });
}

//Post Message to API
function postMesaageApi() {
  const posts = [];

  return db.allDocs({ include_docs: true }).then((docs) => {
    docs.rows.forEach((row) => {
      const doc = row.doc;

      const fetchProm = fetch("api", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(doc),
      }).then((res) => {
        return db.remove(doc);
      });

      posts.push(fetchProm);
    });

    return Promise.all(posts);
  });
}
